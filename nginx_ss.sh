#!/bin/bash
#
#  nginx_ss.sh: modify nginx site status
#  
#  Enables or disables a site using the
#+ sites-available and sites-enabled dirs.
#
#  Usage: nginx_ss.sh enable foo.com
#         nginx_ss.sh disable default

ARGS=2
E_BADARGS=75
E_NO_AVAILABLE_CONF=76
E_NO_ENABLED_CONF=77
E_MISSING_SITE_DIR=78

usage() 
{
    echo "$0: enables or disables a site with nginx"
    echo "using the sites-available and sites-enabled directories."
    echo
    echo "Usage: $0 <enable|disable> <site>"
    echo "Example: $0 enable foo-bar.com"
    echo
    echo "This script does not reload nginx."
    exit $E_BADARGS
}

switch_status() 
{
    case $1 in
        enable)  if [ -e "$AVAILABLE""$2" ]
                 then
                     ln -s "$AVAILABLE""$2" "$ENABLED""$2"
                     exit $?
                 else
                     echo "$0: Error"
                     echo "$AVAILABLE$2 does not exist."
                     echo "First, ensure a config in $AVAILABLE exists for the \"$2\" site."
                     exit $E_NO_AVAILABLE_CONF
                 fi
        ;;
        disable) if [ -e "$ENABLED""$2" ]
                 then
                     rm "$ENABLED""$2"
                     exit $?
                 else
                     echo "$0: Error"
                     echo "$ENABLED$2 does not exist."
                     echo "Hint: it is either not enabled, or enabled elsewhere."
                     exit $E_NO_ENABLED_CONF
                 fi
        ;;
        *)       echo "$1: unrecognized option."
                 exit $E_BADARGS
        ;;     
    esac
}

[[ $# -eq $ARGS ]] || usage

if [[ -e /etc/nginx/sites-available ]] && [[ -e /etc/nginx/sites-enabled ]]
    then
        AVAILABLE=/etc/nginx/sites-available/
        ENABLED=/etc/nginx/sites-enabled/
        switch_status "$@"
    elif [[ -e /usr/local/nginx/conf/sites-available ]] && [[ -e /usr/local/nginx/conf/sites-enabled ]]
        then
        AVAILABLE=/usr/local/nginx/conf/sites-available/
        ENABLED=/usr/local/nginx/conf/sites-enabled/
        switch_status "$@"
    elif
        [[ -e /usr/share/nginx/sites-available ]] && [[ -e /usr/share/nginx/sites-enabled ]]
        then
            AVAILABLE=/usr/share/nginx/sites-available/
            ENABLED=/usr/share/nginx/sites-enabled/
            switch_status "$@"
    elif [[ ! -e /etc/nginx/sites-available ]] || [[ ! -e /etc/nginx/sites-enabled ]]
        then
            echo "Missing a necessary directory."
            echo "Check that both sites-available/ and sites-enabled/ exist."
            exit $E_MISSING_SITE_DIR
    elif [[ ! -e /usr/local/nginx/conf/sites-available ]] || [[ ! -e /usr/local/nginx/conf/sites-enabled ]]
        then
            echo "Missing a necessary directory."
            echo "Check that both sites-available/ and sites-enabled/ exist."
            exit $E_MISSING_SITE_DIR
    elif [[ ! -e /usr/share/nginx/sites-available ]] || [[ ! -e /usr/share/nginx/sites-enabled ]]
        then
            echo "Missing a necessary directory."
            echo "Check that both sites-available/ and sites-enabled/ exist."
            exit $E_MISSING_SITE_DIR
    else
        echo "$0 does not know the location of nginx's enabled sites."
        echo "$0 exiting now."
        exit $E_MISSING_SITE_DIR
fi

exit $?
